FROM puckel/docker-airflow:1.10.9
ENV PYTHONPATH "${PYTHONPATH}:/usr/local/airflow"
USER root
RUN apt-get update
COPY requirements.txt /requirements.txt
RUN pip3 install -r /requirements.txt