import json
import requests

from airflow.models.baseoperator import BaseOperator

class ApiToJson(BaseOperator):
    template_fields = ()
    template_ext = ()
    ui_color = '#ededed'
    
    def __init__(self, out_file, endpoint, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.out_file = out_file
        self.endpoint = endpoint
    
    def execute(self, context):
        raw_data = requests.get(self.endpoint)
        data = raw_data.json()[0]
        
        with open(self.out_file, 'w') as fd:
            json.dump(data, fd)