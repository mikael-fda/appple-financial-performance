import json
import os
import pymongo

import time

#from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.models.baseoperator import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.plugins_manager import AirflowPlugin


class JsonToMongoOperator(BaseOperator):

    template_fields = ()
    template_ext = ()
    ui_color = '#ededed'

    @apply_defaults
    def __init__(self, file_to_load, mongoserver, mongouser, mongopass, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.file_to_load = file_to_load
        self.mongoserver = mongoserver
        self.mongouser = mongouser
        self.mongopass = mongopass

    def execute(self, context):
        MONGO_ENDPOINT = f"mongodb://{self.mongouser}:{self.mongopass}@{self.mongoserver}"
        MONGO_DB = os.environ['MONGO_INITDB_DATABASE']
    
        client = pymongo.MongoClient(MONGO_ENDPOINT)
        table = client[MONGO_DB]['financial']
        
        #run code here to make the connection and
        with open(self.file_to_load, 'r') as fd:
            data = json.load(fd)

        table.insert_one(data)
        client.close()
        
        print("success insert")

class JsonsToMongoOperator(BaseOperator):

    template_fields = ()
    template_ext = ()
    ui_color = '#ededed'

    @apply_defaults
    def __init__(self, files_to_load, mongoserver, mongouser, mongopass, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.files_to_load = files_to_load
        self.mongoserver = mongoserver
        self.mongouser = mongouser
        self.mongopass = mongopass

    def execute(self, context):
        MONGO_ENDPOINT = f"mongodb://{self.mongouser}:{self.mongopass}@{self.mongoserver}"
        MONGO_DB = os.environ['MONGO_INITDB_DATABASE']
    
        client = pymongo.MongoClient(MONGO_ENDPOINT)
        table = client[MONGO_DB]['financial']
        
        #run code here to make the connection and
        data = dict()
        for (k, file) in self.files_to_load:
            with open(file, 'r') as fd:
                data[k] = json.load(fd)

        data['timestamp'] = time.time()
        table.insert_one(data)
        client.close()
        
        print("success insert")


class JsonToMongoPlugin(AirflowPlugin):
    name = "JsonToMongoPlugin"
    operators = [JsonToMongoOperator]
    
