import os
import pprint
import time

import pymongo
import requests


import ipdb

API_KEY = os.environ["FMP_API_KEY"]

PROFILE_ENDPOINT = f"https://financialmodelingprep.com/api/v3/profile/AAPL?apikey={API_KEY}"
RATING_PROFILE = f"https://financialmodelingprep.com/api/v3/rating/AAPL?apikey={API_KEY}"

MAX_RETRY = 2

def get_company_information():

    profiles = requests.get(PROFILE_ENDPOINT)
    profile = profiles.json()[0]

    ratings = requests.get(RATING_PROFILE)
    rating = ratings.json()[0]

    data = {
        "profile": profile,
        "rating": rating,
        "timestamp": time.time()
    }
    pprint.pprint(data) 

    return data

def mongo_db():
    MONGO_USER = os.environ['MONGO_INITDB_ROOT_USERNAME']
    MONGO_PWD = os.environ['MONGO_INITDB_ROOT_PASSWORD']
    MONGO_DB = os.environ['MONGO_INITDB_DATABASE']
    
    MONGO_ENDPOINT = f"mongodb://{MONGO_USER}:{MONGO_PWD}@mongo"
    client = pymongo.MongoClient(MONGO_ENDPOINT)
    db = client[MONGO_DB]
    return client, db['financial']

def insert_data():
    data = get_company_information()
    client, db = mongo_db()
    db.insert_one(data)
    client.close() 
    

if __name__ == "__main__":
    insert_data()
