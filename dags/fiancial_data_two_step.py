import json
import pprint

from fiancial_data import get_company_information, mongo_db

TMP_FILE = "/tmp/data.json"

def get_company_information_json():
    data = get_company_information()
    with open(TMP_FILE, 'w') as fd:
        json.dump(data, fd)

    return data

def insert_from_file():
    with open(TMP_FILE, 'r') as fd:
        data = json.load(fd)
    pprint.pprint(data)

    client, db = mongo_db()
    db.insert_one(data)
    client.close() 


    
