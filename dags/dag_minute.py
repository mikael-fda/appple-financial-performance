import airflow
import os

from datetime import timedelta

from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from dags.fiancial_data import insert_data
from dags.fiancial_data_two_step import get_company_information_json, insert_from_file, TMP_FILE
from plugins.jsontomongo import JsonToMongoOperator, JsonsToMongoOperator
from plugins.apitojson import ApiToJson

args = {
    "id": "minute_dag",
    "owner": "mikael",
    "start_date": airflow.utils.dates.days_ago(0),
    'retries': 1,
    'retry_delay': timedelta(seconds=10),
}

dag = DAG(
    dag_id='minute_dag',
    default_args=args,
    description='A simple DAG',
    schedule_interval= "0 * * * *"
)

t1 = PythonOperator(
    task_id="minute_financial",
    python_callable=insert_data,
    dag=dag
)
t11 = PythonOperator(
    task_id="two_step_write",
    python_callable=get_company_information_json,
    dag=dag
)
t12 = PythonOperator(
    task_id="two_step_insert",
    python_callable=insert_from_file,
    dag=dag
)

MONGO_USER = os.environ['MONGO_INITDB_ROOT_USERNAME']
MONGO_PWD = os.environ['MONGO_INITDB_ROOT_PASSWORD']

t13= JsonToMongoOperator(
    task_id='two_step_operator',
    file_to_load=TMP_FILE,
    mongoserver="mongo",
    mongouser=MONGO_USER,
    mongopass=MONGO_PWD,
    dag=dag,
)

from dags.fiancial_data import PROFILE_ENDPOINT, RATING_PROFILE

TMP_PROFILE = "/tmp/profile.json"
TMP_RATING = "/tmp/rating.json"


t21 = ApiToJson(task_id="fetch_profile", out_file=TMP_PROFILE, endpoint=PROFILE_ENDPOINT, dag=dag)
t22 = ApiToJson(task_id="fetch_rating", out_file=TMP_RATING, endpoint=RATING_PROFILE, dag=dag)
t23 = JsonToMongoOperator(task_id='write_profile', file_to_load=TMP_PROFILE, mongoserver="mongo", mongouser=MONGO_USER, mongopass=MONGO_PWD, dag=dag,)
t24 = JsonToMongoOperator(task_id='write_rating', file_to_load=TMP_RATING, mongoserver="mongo", mongouser=MONGO_USER, mongopass=MONGO_PWD, dag=dag,)

t25 = JsonsToMongoOperator(
    task_id="write_both",
    files_to_load=[('profile', TMP_PROFILE), ('rating', TMP_RATING)],
    mongoserver="mongo",
    mongouser=MONGO_USER,
    mongopass=MONGO_PWD,
    dag=dag
)


t1
t11 >> t12
t11 >> t13
t21 >> t23
t22 >> t24
t21 >> t22 >> t25